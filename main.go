package main

import (
	b64 "encoding/base64"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"

	"github.com/xanzy/go-gitlab"
)

func getGitlab() *gitlab.Client {
	url := os.Getenv("GITLAB_URL")
	token := os.Getenv("GITLAB_TOKEN")
	gl, err := gitlab.NewClient(token, gitlab.WithBaseURL(url))
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	return gl
}

func getNewFileContent(localFilePath string) string {
	contents, err := os.ReadFile(localFilePath)
	if err != nil {
		log.Fatal("Could not open file")
	}
	return string(contents)
}

type args struct {
	file           string
	repo           string
	repoFile       string
	commitMessage  string
	force          bool
	branchName     string
	findPattern    string
	replaceContent string
}

func parseArgs() *args {
	fileFlag := flag.String("file", "", "Specify the location of the local file")
	repoFlag := flag.String("repo", "", "The location of the GitLab repo. E.g. 'gitlab-org/gitlab'")
	repoFileFlag := flag.String("repo-file", "", "The repository file to replace")
	branchNameFlag := flag.String("branch", "", "The branch name to use. Optional. Defaults to refactor-change-file-<filename>")
	commitMessageFlag := flag.String("m", "", "The commit message")
	findPatternFlag := flag.String("find-pattern", "", "The regex pattern to find")
	replaceContentFlag := flag.String("replace-content", "", "The regex pattern to replace")
	forceFlag := flag.Bool("force", false, "Force branch creation")
	flag.Parse()
	arguments := args{}
	arguments.file = *fileFlag
	arguments.repo = *repoFlag
	arguments.repoFile = *repoFileFlag
	arguments.commitMessage = *commitMessageFlag
	arguments.findPattern = *findPatternFlag
	arguments.replaceContent = *replaceContentFlag
	arguments.force = *forceFlag
	if *branchNameFlag == "" {
		arguments.branchName = fmt.Sprintf("refactor-change-file-%s", filepath.Base(*repoFileFlag))
	} else {
		arguments.branchName = *branchNameFlag
	}
	if arguments.repo == "" || arguments.repoFile == "" || arguments.commitMessage == "" {
		log.Fatalln("repo, repo-file and commitMessage (-m) are required.")
	}
	if (arguments.replaceContent != "" || arguments.findPattern != "") && arguments.file != "" {
		log.Fatalln("Cannot operate in regex and file replace mode at the same time.")
	}

	return &arguments
}

func getBranchOptions(branchName string, defaultBranch string) *gitlab.CreateBranchOptions {
	return &gitlab.CreateBranchOptions{Branch: &branchName, Ref: &defaultBranch}
}

func getDefaultBranch(gl *gitlab.Client, repo string) string {
	project, _, err := gl.Projects.GetProject(repo, nil, nil)
	if err != nil {
		log.Fatalln(err)
	}
	return project.DefaultBranch
}

func createBranch(gl *gitlab.Client, branchName string, defaultBranch string, arguments *args) {
	if arguments.force {
		gl.Branches.DeleteBranch(arguments.repo, branchName)
	}
	_, _, err := gl.Branches.CreateBranch(arguments.repo, getBranchOptions(branchName, defaultBranch))
	if err != nil {
		fmt.Println(fmt.Sprintf("Could not create branch '%s'. Does it already exist? Use -force to ignore.", branchName))
		log.Fatalln(err)
	}
}

func updateRepositoryFile(gl *gitlab.Client, arguments *args, fileContent string) {
	options := &gitlab.UpdateFileOptions{
		Branch:        gitlab.String(arguments.branchName),
		Content:       gitlab.String(fileContent),
		CommitMessage: gitlab.String(arguments.commitMessage),
	}
	_, _, err := gl.RepositoryFiles.UpdateFile(arguments.repo, arguments.repoFile, options)
	if err != nil {
		log.Fatalln(err)
	}
}

func getNewRegexFileContent(gl *gitlab.Client, defaultBranch string, arguments *args) string {
	fileOptions := &gitlab.GetFileOptions{
		Ref: &defaultBranch,
	}
	oldFile, _, err := gl.RepositoryFiles.GetFile(arguments.repo, arguments.repoFile, fileOptions, nil)
	if err != nil {
		log.Fatalln(err)
	}
	oldFileContent, _ := b64.StdEncoding.DecodeString(oldFile.Content)
	re := regexp.MustCompile(arguments.findPattern)
	newContent := re.ReplaceAll([]byte(oldFileContent), []byte(arguments.replaceContent))
	return string(newContent)
}

func main() {
	arguments := parseArgs()
	gl := getGitlab()
	defaultBranch := getDefaultBranch(gl, arguments.repo)
	createBranch(gl, arguments.branchName, defaultBranch, arguments)
	var newContent string
	if arguments.file != "" {
		newContent = getNewFileContent(arguments.file)
	} else if arguments.findPattern != "" && arguments.replaceContent != "" {
		newContent = getNewRegexFileContent(gl, defaultBranch, arguments)
	}
	updateRepositoryFile(gl, arguments, newContent)
	gl.MergeRequests.CreateMergeRequest(
		arguments.repo,
		&gitlab.CreateMergeRequestOptions{
			Title:              &arguments.commitMessage,
			SourceBranch:       &arguments.branchName,
			TargetBranch:       &defaultBranch,
			RemoveSourceBranch: gitlab.Bool(true),
		},
	)
}
