# gl-file-replacer

Tool to replace files in repositories in Gitlab via the merge request flow.

Can be used to automate the file replacements in many repositories.

## Usage

```
Usage of ./gl-file-replacer:
  -branch string
        The branch name to use. Optional. Defaults to refactor-change-file-<filename>
  -file string
        Specify the location of the local file
  -find-pattern string
        The regex pattern to find
  -force
        Force branch creation
  -m string
        The commit message
  -replace-content string
        The regex pattern to replace
  -repo string
        The location of the GitLab repo. E.g. 'gitlab-org/gitlab'
  -repo-file string
        The repository file to replace
```

### File replace mode

Works by replacing the remote file content with the local one using the `-file` flag.

#### Example to replace the README.md in the repository with a local one

```sh
gl-file-replacer -repo max-wittig/gl-file-replacer -repo-file README.md -file ../other-repo/README.md -m "chore: update readme"
```

### Regex replace mode

Replaces a certain pattern in the remote file with specified replacement content using the `-find-pattern` and `replace-content` flags.

#### Example to replace the a string in the README.md file in the repository with a specified different string

```sh
gl-file-replacer -repo max-wittig//gl-file-replacer -repo-file README.md --find-pattern "Install on your platform" --replace-content "Install on your machine" -m "chore: update readme" -force
```

Causes this diff to happen for this `README.md` file:

```diff
-Install on your platform
+Install on your machine
```

### Build

Install on your platform

```sh
make install
```

Build for all available platforms

```sh
make build-all
```
